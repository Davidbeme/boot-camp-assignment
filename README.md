# Boot Camp Application Assignment #

run.py is a Python3 file and displays output using the print() function.

### What does this project do? ###

* Scans https://banno.com/ and returns information about it.

### How do I use it? ###

* Install Python3
* Download the repo
* Make sure that the **bs4** folder is in the same directory as **run.py**
* Execute **run.py** from the command line