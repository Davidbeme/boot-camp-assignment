#!/usr/bin/env python3
'''Banno Boot Camp Application Assignment

This application aims to collect the required information from
https://banno.com/  and display it.

The BeautifulSoup library was used to easily obtain that
information.  It's project page can be found at
https://www.crummy.com/software/BeautifulSoup/

Author: David Williams
'''

import string, urllib.request
from bs4 import BeautifulSoup

def count_3_most_chars(alphanum_str):
    '''Gets the 3 most occuring chars and their counts'''
    #We only want alphanumerics
    chars = string.ascii_letters + string.digits
    
    #Build a dictionary of chars and their counts
    char_dict = {}
    for ch in chars:
        char_dict[ch] = alphanum_str.count(ch)
        
    #We'll be removing values, so two instances
    modified_vals = list(char_dict.values())
    vals = list(char_dict.values())
    keys = list(char_dict.keys())

    #Get the most occuring, then remove it, repeat
    most_1 = max(modified_vals)
    modified_vals.remove(most_1)
    most_2 = max(modified_vals)
    modified_vals.remove(most_2)
    most_3 = max(modified_vals)

    #Return 3 tuples of the format (char, amt)
    return ((keys[vals.index(most_1)], most_1), \
            (keys[vals.index(most_2)], most_2), \
            (keys[vals.index(most_3)], most_3))
    

def main():
    req = urllib.request.Request('https://banno.com/')
    response = urllib.request.urlopen(req)

    soup = BeautifulSoup(response.read(), 'html.parser')

    #A count of the number of Platform features
    platformItems = []
    for col in soup.find_all("div", { "class" : "column" }):
    	platformItems.append(col.h3.get_text().strip())

    print("Found", len(platformItems), "Platform Features in the HTML:")

    for i in platformItems:
    	print(" ", i)

    print()

    #Get the three most reoccuring alphanum chars (case dependent)
    three_most = count_3_most_chars(soup.prettify())
    print("The top 3 characters are " + str(three_most[0][0]) + ",", \
          str(three_most[1][0]) + ", and", \
          three_most[2][0], "with the following occurences:")

    for pair in three_most:
        print(" ", pair[0], "-", pair[1])

    print()

    #The number of .png images in the HTML (as well as their urls)
    pngURLs = []
    for img in soup.find_all('img'):
        imgPath = img.get('src')
        if imgPath[-3:] == "png":
            pngURLs.append(imgPath)

    print("Found", len(pngURLs), "PNGs in the HTML:")

    for i in pngURLs:
        print(" ", i)

    print()

    #BannoJHA's Twitter handle
    twitter_raw = soup.find(type="application/ld+json").get_text()
    twitter_index_begin = twitter_raw.index("twitter.com") + 12
    twitter_index_end = twitter_raw.index("\"", twitter_index_begin)
    twitter_handle = twitter_raw[twitter_index_begin:twitter_index_end]

    print("BannoJHA's Twitter Handle:", twitter_handle)

    print()

    #Amount of occurences of Financial Institution
    occurFinInst = soup.get_text().lower().count("financial institution")
    print("The term \"financial institution\" occurs", occurFinInst, "times.")

main()
